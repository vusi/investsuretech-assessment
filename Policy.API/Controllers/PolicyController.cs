﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Policy.Domain.Models;
using Policy.InsuranceService.Interfaces;

namespace Policy.API.Controllers
{
    [Route("api/policy")]
    [ApiController]
    public class PolicyController : ControllerBase
    {
        private readonly IInsuranceService _insuranceService;

        public PolicyController(IInsuranceService insuranceService)
        {
            _insuranceService = insuranceService;
        }

        [HttpGet]
		[Route("api/GetPolicy")]
        public async Task<IActionResult> GetPolicy(string idNumber)
        {
            try
            {
                var item = await _insuranceService.GetPolicyByCustomerIdNumber(idNumber);

                if (item == null)
                {
                    return NotFound();
                }

                return Ok(item);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewPolicy([FromBody] PolicyEntity newPolicy)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var success = await _insuranceService.CreateNewPolicy(newPolicy);
                if (!success)
                {
                    return StatusCode(424, "Failed Dependency");
                }

                return StatusCode(201, "Created.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

		[HttpGet]
		[Route("api/GetRiskItemsByPolicy")]
		public async Task<IActionResult> GetRiskItemsByPolicy(string policyNumber)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				var riskItems = await _insuranceService.GetRiskItemsByPolicyNumber(policyNumber);
				if (!riskItems.Any())
				{
					return NotFound();
				}

				return Ok(riskItems);
			}
			catch (Exception ex)
			{
				return StatusCode(500, "Internal server error");
			}
		}
	}
}
﻿using Microsoft.Extensions.DependencyInjection;
using Policy.InsuranceService.Interfaces;
using Swashbuckle.AspNetCore.Swagger;

namespace Policy.API.ServiceExtensions
{
    public static class ServiceExtension
    {
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Policy API", Version = "v1" });
            });
        }

        public static void ConfigureInversionOfControl(this IServiceCollection services)
        {
            services.AddScoped<IInsuranceService, InsuranceService.Implementation.InsuranceService>();
        }
    }
}

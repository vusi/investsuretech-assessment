﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Policy.Domain.Models
{
    public class PolicyEntity
    {
        public string CustomerIdNumber { get; set; }
        public DateTime DebitOrderDate { get; set; }
        public DateTime PolicyInceptionDate { get; set; }
        public string PolicyNumber { get; set; }
        public bool PolicyStatus { get; set; }
        public decimal Premium { get; set; }
        public DateTime TransactionDate { get; set; }
        public ICollection<RiskItemEntity> RiskItems { get; set; }

        public decimal GetTotalPremium()
        {
            return RiskItems.Sum(x => x.Premium);
        }

        public bool IsInceptionDateBeforeToday()
        {
            return PolicyInceptionDate >= DateTime.Today;
        }

        public bool IsPremiumLessThanLimit(decimal limit)
        {
            return Premium <= limit;
        }
    }
}

﻿using System;

namespace Policy.Domain.Models
{
    public class CustomerEntity
    {
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string ZaId { get; set; }
        public DateTime DateOfBirth
        {
            get
            {
                var year = Convert.ToInt32(ZaId.Substring(0, 2));
                var month = Convert.ToInt32(ZaId.Substring(2, 2));
                var day = Convert.ToInt32(ZaId.Substring(4, 2));
                return new DateTime(year, month, day);
            }
        }
        public bool IsValidIdNumber()
        {
            return ZaId.Length == 13;
        }
    }
}

﻿using System;

namespace Policy.Domain.Models
{
    public class RiskItemEntity
    {
        public decimal Premium { get; set; }
        public string RiskItemName { get; set; }
        public DateTime RiskStartDate { get; set; }
        public PolicyEntity Policy { get; set; }

        public bool IsRiskStartDateBeforeThanPolicyStartDate()
        {
            return RiskStartDate < Policy.PolicyInceptionDate;
        }
    }
}

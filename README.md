# Policy API

Policy is an ASP.NET Core Web API for insurance company that manages the querying and creation of insurance products

## Development Approach

This project was developed using some of Agile principles and practices. Reason for Agile is:

```
1. Agile has fast and flexible processes that increases productivity.
2. Agile helps teams and individuals effectively prioritize work and features.
3. Teams can make quick-course corrections based on stakeholder feedback.
4. Teams get rapid feedback from each version or iteration.
```

## Development Assumptions Made

```
1. That the API has authentication configured using either Asp.Net Identity or JWT
```

## Clone project

```bash
git clone https://gitlab.com/vusi/investsuretech-assessment.git
```

## Try it out locally

This API is documented using [Swashbuckle](https://swagger.io/), an open source project for generating Swagger documents for ASP.NET [Core] Web APIs. 

```bash
>> clone project
>> cd Policy.API
>> dotnet build
>> dotnet run

Running Unit Tests

>> cd ..
>> cd Policy.Tests
>> dotnet test

```

## Continuous Integration Using (Gitlab CI)

The project is setup to continuous run integration build and unit tests whenever you push to master branch.

## Continuous Deployment using (Azure App Services)

The project is setup to deploy the web api to azure app service on success of CI.

```python
https://insurancepolicy.azurewebsites.net/index.html
```


## License
[MIT](https://choosealicense.com/licenses/mit/)
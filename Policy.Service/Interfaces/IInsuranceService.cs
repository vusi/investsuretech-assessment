﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Policy.Domain.Models;

namespace Policy.InsuranceService.Interfaces
{
    public interface IInsuranceService
    {
        Task<bool> CreateNewPolicy(PolicyEntity newPolicy);
        Task<PolicyEntity> GetPolicyByCustomerIdNumber(string idNumber);
        Task<IEnumerable<RiskItemEntity>> GetRiskItemsByPolicyNumber(string policyNumber);
    }
}

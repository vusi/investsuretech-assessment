﻿using System;
using System.Net.Http;

namespace Policy.InsuranceService.RestClient
{
    public class HttpRestClient
    {
        private const string Url = "http:?/api";

        public string GetResource(string action)
        {
            using (var httpClient = new HttpClient())
            {
                // Add headers

                httpClient.BaseAddress = new Uri(Url);
                var response = httpClient.GetStringAsync($"/{action}").Result;
                
                return response;
            }
        }

        public string PostResource(string action, dynamic resource)
        {
            using (var httpClient = new HttpClient())
            {
                // Add headers

                httpClient.BaseAddress = new Uri(Url);
                var response = httpClient.PostAsync($"/{action}", resource).Result;

                return response;
            }
        }
    }
}

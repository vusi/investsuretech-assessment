﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Policy.Domain.Models;
using Policy.InsuranceService.Interfaces;

namespace Policy.InsuranceService.Implementation
{
	public class InsuranceService : IInsuranceService
	{
		public async Task<bool> CreateNewPolicy(PolicyEntity newPolicy)
		{
			//var restClient = new HttpRestClient();
			//var response = restClient.PostResource("PolicyResource);

			return await Task.FromResult(true);
		}

		public async Task<PolicyEntity> GetPolicyByCustomerIdNumber(string idNumber)
		{
			//var restClient = new HttpRestClient();
			//var response = restClient.GetResource("PolicyResource");

			var policy = new PolicyEntity
			{
				PolicyNumber = "ABC123"
			};

			return await Task.FromResult(policy);
		}

		public async Task<IEnumerable<RiskItemEntity>> GetRiskItemsByPolicyNumber(string policyNumber)
		{
			//var restClient = new HttpRestClient();
			//var response = restClient.GetResource("RiskItemResource");

			var riskItems = new List<RiskItemEntity>
			{
				new RiskItemEntity
				{
					RiskItemName = "Private Vehicle"
				},
				new RiskItemEntity
				{
					RiskItemName = "Household"
				},
			};

			return await Task.FromResult(riskItems);
		}
	}
}
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Policy.API.Controllers;
using Policy.Domain.Models;
using Policy.InsuranceService.Interfaces;
using Xunit;

namespace Policy.Tests
{
	public class ApiTests
	{
		private PolicyController _policyController;
		private readonly Mock<IInsuranceService> _insuranceService;

		public ApiTests()
		{
			_insuranceService = new Mock<IInsuranceService>();
		}

		[Fact]
		public void For_GetPolicyByIdNumber_GivenInvalidNumber_ShouldReturn_NotFoundResult()
		{
			//Arrange
			_insuranceService.Setup(x => x.GetPolicyByCustomerIdNumber(It.IsAny<string>())).Returns(Task.FromResult<PolicyEntity>(null));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var notFoundResult = _policyController.GetPolicy("0000000000000").Result as NotFoundResult;

			//Assert
			Assert.IsType<NotFoundResult>(notFoundResult);
		}

		[Fact]
		public void For_GetPolicyByIdNumber_GivenAValidNumber_ShouldReturn_PolicyDetails()
		{
			//Arrange
			_insuranceService.Setup(x => x.GetPolicyByCustomerIdNumber(It.IsAny<string>())).Returns(Task.FromResult(new PolicyEntity { PolicyNumber = "ABC123" }));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var foundResults = _policyController.GetPolicy("8802015755085").Result as OkObjectResult;

			//Assert
			Assert.IsType<OkObjectResult>(foundResults);
			Assert.IsType<PolicyEntity>(foundResults.Value);
		}

		[Fact]
		public void For_CreateNewPolicy_GivenInvalidModel_ShouldReturn_BadRequestResult()
		{
			//Arrange
			_insuranceService.Setup(x => x.GetPolicyByCustomerIdNumber(It.IsAny<string>())).Returns(Task.FromResult(new PolicyEntity { PolicyNumber = "ABC123" }));
			_policyController = new PolicyController(_insuranceService.Object);
			_policyController.ModelState.AddModelError("RiskItem", "Risk Item Is Required");

			//Act
			var createResults = _policyController.CreateNewPolicy(new PolicyEntity()).Result as BadRequestObjectResult;

			//Assert
			Assert.IsType<BadRequestObjectResult>(createResults);
		}

		[Fact]
		public void For_CreateNewPolicy_GivenValidModel_ShouldReturn_OkResult()
		{
			//Arrange
			_insuranceService.Setup(x => x.CreateNewPolicy(It.IsAny<PolicyEntity>())).Returns(Task.FromResult(true));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var createResults = _policyController.CreateNewPolicy(new PolicyEntity()).Result as ObjectResult;

			//Assert
			Assert.Equal("Created.", createResults?.Value);
		}

		[Fact]
		public void For_CreateNewPolicy_GivenDomainErrors_ShouldReturn_FailedDependency()
		{
			//Arrange
			_insuranceService.Setup(x => x.CreateNewPolicy(It.IsAny<PolicyEntity>())).Returns(Task.FromResult(false));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var createResults = _policyController.CreateNewPolicy(new PolicyEntity()).Result as ObjectResult;

			//Assert
			Assert.Equal("Failed Dependency", createResults?.Value);
		}

		[Fact]
		public void For_GetRiskItemsByPolicy_GivenPolicyNumber_ShouldReturn_RiskTypes()
		{
			//Arrange
			var items = new List<RiskItemEntity>
			{
				new RiskItemEntity
				{
					RiskItemName = "Private Vehicle"
				},
				new RiskItemEntity
				{
					RiskItemName = "Household"
				},
			};
			_insuranceService.Setup(x => x.GetRiskItemsByPolicyNumber(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<RiskItemEntity>>(items));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var response = _policyController.GetRiskItemsByPolicy("ABC123").Result as OkObjectResult;

			//Assert
			Assert.IsType<OkObjectResult>(response);
		}

		[Fact]
		public void For_GetRiskItemsByPolicy_GivenPolicyNumberWithNoRiskItems_ShouldReturn_NotFoundObjectResults()
		{
			//Arrange
			_insuranceService.Setup(x => x.GetRiskItemsByPolicyNumber(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<RiskItemEntity>>(new List<RiskItemEntity>()));
			_policyController = new PolicyController(_insuranceService.Object);

			//Act
			var response = _policyController.GetRiskItemsByPolicy("ABC123").Result as NotFoundResult;

			//Assert
			Assert.IsType<NotFoundResult>(response);
		}
	}
}